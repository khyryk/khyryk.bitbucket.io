"use strict";

const regex = /(\w+) \| (.+) \| (.+)/;

function search() {
    let survivor = document.querySelector('input[name="survivor"]:checked').value;
    let select = document.getElementById("select");
    select.innerHTML = ""; // delete all existing
    let text = document.getElementById("search").value;
    list.forEach((line) => {
        if (survivor !== "all") {
            if (survivor === line.match(regex)[1]
                && line.toLowerCase().includes(text.toLowerCase())) {
                let opt = document.createElement("option");
                opt.value = line.match(regex)[2];
                opt.innerHTML = line;
                select.appendChild(opt);
            }
        }
        else if (line.toLowerCase().includes(text.toLowerCase())) {
            let opt = document.createElement("option");
            opt.value = line.match(regex)[2];
            opt.innerHTML = line;
            select.appendChild(opt);
        }
    });
}

function echo() {
    document.getElementById("echo").innerHTML = document.getElementById("select").value;
}
